import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

public class SqlData {

    //插入变量
    static String dt;
    static String region;
    static String dau;
    static String nru;
    static String businessnru;
    static String das;
    static String diu;
    static String ltv30;
    static String firstrecharge;
    static String D1;
    static String D3;
    static String D7;
    static String D15;
    static String D30;
    static String DayCoinRevenuue;
    static String DayGameCoinRevenuue;
    static String DayCoinPoints;
    static String DayGameCoinPoints;
    static String DayCoinUser;
    static String DayGameCoinUser;
    static String DayCoinArppu;
    static String MonCoinRevenue;
    static String MonCoinPoints;
    static String MonCoinUser;
    static String MonCoinArppu;
    static String DayCashRevenue;
    static String DayCashPoints;
    static String DayCashUser;
    static String DayCashArppu;
    static String MonCashRevenuue;
    static String MonCashPoints;
    static String MonCashUser;
    static String MonCashArppu;







    static final String redshift_dau_sql = "select * from bi_dws.dws_wide_index_report_d where date=";

    static final String mysql_insert_sql =
            "insert into ads_rpt_work_d(date,region,dau,nru,businessnru,das,diu,ltv30,firstrecharge," +
                    "D1,D3,D7,D15,D30,DayCoinRevenuue,DayGameCoinRevenuue,DayCoinPoints,DayGameCoinPoints,DayCoinUser," +
                    "DayGameCoinUser,DayCoinArppu,MonCoinRevenue,MonCoinPoints,MonCoinUser,MonCoinArppu,DayCashRevenue," +
                    "DayCashPoints,DayCashUser,DayCashArppu,MonCashRevenuue,MonCashPoints,MonCashUser,MonCashArppu) " +
                    "values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";


    static final String mysql_delete_sql =
            "delete from ads_rpt_work_d where date=";

    static final String mysql_insert_sql_isnull = "insert into ads_rpt_work_d(date,region,dau) values(?,?,?)";

    public static String query_redshift(String  date) {
        return redshift_dau_sql + "'" + date + "'";
    }


    public static String delete_mysql(String  date) {
        return mysql_delete_sql + "'" + date + "'";
    }



    //获取get变量
    public static void variable_get_definitions(ResultSet resultSet) throws Exception{
        SqlData.dt =
                resultSet.getObject("date").toString();
        SqlData.region =
                resultSet.getObject("region").toString();
        SqlData.dau =
                resultSet.getObject("dau").toString();
        SqlData.nru =
                resultSet.getObject("nru").toString();
        SqlData.businessnru =
                resultSet.getObject("businessnru").toString();
        SqlData.das =
                resultSet.getObject("das").toString();
        SqlData.diu =
                resultSet.getObject("diu").toString();
        SqlData.ltv30 =
                resultSet.getObject("ltv30").toString();
        SqlData.firstrecharge =
                resultSet.getObject("firstrecharge").toString();
        SqlData.D1 =
                resultSet.getObject("D1").toString();
        SqlData.D3 =
                resultSet.getObject("D3").toString();
        SqlData.D7 =
                resultSet.getObject("D7").toString();
        SqlData.D15 =
                resultSet.getObject("D15").toString();
        SqlData.D30 =
                resultSet.getObject("D30").toString();
        SqlData.DayCoinRevenuue =
                resultSet.getObject("DayCoinRevenuue").toString();
        SqlData.DayGameCoinRevenuue =
                resultSet.getObject("DayGameCoinRevenuue").toString();
        SqlData.DayCoinPoints =
                resultSet.getObject("DayCoinPoints").toString();
        SqlData.DayGameCoinPoints =
                resultSet.getObject("DayGameCoinPoints").toString();
        SqlData.DayCoinUser =
                resultSet.getObject("DayCoinUser").toString();
        SqlData.DayGameCoinUser =
                resultSet.getObject("DayGameCoinUser").toString();
        SqlData.DayCoinArppu =
                resultSet.getObject("DayCoinArppu").toString();
        SqlData.MonCoinRevenue =
                resultSet.getObject("MonCoinRevenue").toString();
        SqlData.MonCoinPoints =
                resultSet.getObject("MonCoinPoints").toString();
        SqlData.MonCoinUser =
                resultSet.getObject("MonCoinUser").toString();
        SqlData.MonCoinArppu =
                resultSet.getObject("MonCoinArppu").toString();
        SqlData.DayCashRevenue =
                resultSet.getObject("DayCashRevenue").toString();
        SqlData.DayCashPoints =
                resultSet.getObject("DayCashPoints").toString();
        SqlData.DayCashUser =
                resultSet.getObject("DayCashUser").toString();
        SqlData.DayCashArppu =
                resultSet.getObject("DayCashArppu").toString();
        SqlData.MonCashRevenuue =
                resultSet.getObject("MonCashRevenuue").toString();
        SqlData.MonCashPoints =
                resultSet.getObject("MonCashPoints").toString();
        SqlData.MonCashUser =
                resultSet.getObject("MonCashUser").toString();
        SqlData.MonCashArppu =
                resultSet.getObject("MonCashArppu").toString();
    }

    //获取set变量
    public static void variable_set_definitions(PreparedStatement preparedStatement) throws Exception{
        preparedStatement.setObject(1, SqlData.dt);
        preparedStatement.setObject(2, SqlData.region);
        preparedStatement.setObject(3, SqlData.dau);
        preparedStatement.setObject(4, SqlData.nru);
        preparedStatement.setObject(5, SqlData.businessnru);
        preparedStatement.setObject(6, SqlData.das);
        preparedStatement.setObject(7, SqlData.diu);
        preparedStatement.setObject(8, SqlData.ltv30);
        preparedStatement.setObject(9, SqlData.firstrecharge);
        preparedStatement.setObject(10, SqlData.D1);
        preparedStatement.setObject(11, SqlData.D3);
        preparedStatement.setObject(12, SqlData.D7);
        preparedStatement.setObject(13, SqlData.D15);
        preparedStatement.setObject(14, SqlData.D30);
        preparedStatement.setObject(15, SqlData.DayCoinRevenuue);
        preparedStatement.setObject(16, SqlData.DayGameCoinRevenuue);
        preparedStatement.setObject(17, SqlData.DayCoinPoints);
        preparedStatement.setObject(18, SqlData.DayGameCoinPoints);
        preparedStatement.setObject(19, SqlData.DayCoinUser);
        preparedStatement.setObject(20, SqlData.DayGameCoinUser);
        preparedStatement.setObject(21, SqlData.DayCoinArppu);
        preparedStatement.setObject(22, SqlData.MonCoinRevenue);
        preparedStatement.setObject(23, SqlData.MonCoinPoints);
        preparedStatement.setObject(24, SqlData.MonCoinUser);
        preparedStatement.setObject(25, SqlData.MonCoinArppu);
        preparedStatement.setObject(26, SqlData.DayCashRevenue);
        preparedStatement.setObject(27, SqlData.DayCashPoints);
        preparedStatement.setObject(28, SqlData.DayCashUser);
        preparedStatement.setObject(29, SqlData.DayCashArppu);
        preparedStatement.setObject(30, SqlData.MonCashRevenuue);
        preparedStatement.setObject(31, SqlData.MonCashPoints);
        preparedStatement.setObject(32, SqlData.MonCashUser);
        preparedStatement.setObject(33, SqlData.MonCashArppu);

    }

}
