import util.Mysql_JDBCUtil;
import util.RedShift_JDBCUtil;

import java.sql.*;

public class DwstoAds {


    static PreparedStatement preparedStatement = null;
    static Connection connection = null;
    static Connection redshift_connection = null;
    //



    public static void main(String[] args) throws Exception {
        String times=args[0];
        result_to_mysql(times);
    }


    public static ResultSet query_redshift(String sql) throws Exception {
        System.out.println(sql+"=====");
        redshift_connection = RedShift_JDBCUtil.getConnection();
        preparedStatement = redshift_connection.prepareStatement(sql);
        return preparedStatement.executeQuery();
    }




    /**
     * 插入数据
     *
     * @throws Exception
     */
    public static void result_to_mysql(String  times) throws Exception {
        ResultSet resultSet = query_redshift(SqlData.query_redshift(times));
        //获取connection对象
        connection = Mysql_JDBCUtil.getConnection();
        //先删除
        preparedStatement = connection.prepareStatement(SqlData.delete_mysql(times));
        int delete = preparedStatement.executeUpdate();
        System.out.println("delete result---"+delete);
        if(resultSet!=null){
            while (resultSet.next()) {
                SqlData.variable_get_definitions(resultSet);
                preparedStatement = connection.prepareStatement(SqlData.mysql_insert_sql);
                System.out.println(SqlData.mysql_insert_sql);
                SqlData.variable_set_definitions(preparedStatement);
                int i = preparedStatement.executeUpdate();
                System.out.println(i);
            }
        }else {
            preparedStatement = connection.prepareStatement(SqlData.mysql_insert_sql_isnull);
        }

        Mysql_JDBCUtil.close(preparedStatement, connection);
    }


}
