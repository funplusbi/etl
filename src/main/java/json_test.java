import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import util.RedShift_JDBCUtil;

import java.io.*;
import java.sql.Connection;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class json_test {

    public static void main(String[] args) {

        //标示符
        String flag=args[0];
        String times=args[1];
        ArrayList<String> sqllist = new ArrayList<>();
        ArrayList<String> sqllist_insert = new ArrayList<>();
        String path_delete="/mnt/yue/json_file/sql.json";
        String path_insert="/mnt/yue/json_file/sql.json";
        String delete_data = readJsonFile(path_delete);
        String insert_data = readJsonFile(path_insert);
        JSONObject json_delete = JSON.parseObject(delete_data);
        String delete_sql_dwd = json_delete.getString("delete_sql_dwd").replace("'?'", times);
        String delete_sql_dws = json_delete.getString("delete_sql_dws").replace("'?'", times);
        sqllist.add(delete_sql_dwd);
        sqllist.add(delete_sql_dws);

        JSONObject json_insert = JSON.parseObject(insert_data);
        String insert_sql_dau = json_insert.getString("insert_sql_dau").replace("'?'", times);
        String insert_sql_nru = json_insert.getString("insert_sql_nru").replace("'?'", times);
        String insert_sql_bnsdau = json_insert.getString("insert_sql_bnsdau").replace("'?'", times);
        String insert_sql_das = json_insert.getString("insert_sql_das").replace("'?'", times);
        String insert_sql_diu = json_insert.getString("insert_sql_diu").replace("'?'", times);
        String insert_sql_30ltv = json_insert.getString("insert_sql_30ltv").replace("'?'", times);
        String insert_sql_FirstRecharge = json_insert.getString("insert_sql_FirstRecharge").replace("'?'", times);
        String insert_sql_d1 = json_insert.getString("insert_sql_d1").replace("'?'", times);
        String insert_sql_d3 = json_insert.getString("insert_sql_d3").replace("'?'", times);
        String insert_sql_d7 = json_insert.getString("insert_sql_d7").replace("'?'", times);
        String insert_sql_d15 = json_insert.getString("insert_sql_d15").replace("'?'", times);
        String insert_sql_d30 = json_insert.getString("insert_sql_d30").replace("'?'", times);
        String insert_sql_DayCoinRevenuue = json_insert.getString("insert_sql_DayCoinRevenuue").replace("'?'", times);
        String insert_sql_DayGameCoinRevenuue=json_insert.getString("insert_sql_DayGameCoinRevenuue").replace("'?'", times);
        String insert_sql_DayCoinPoints = json_insert.getString("insert_sql_DayCoinPoints").replace("'?'", times);
        String insert_sql_DayGameCoinPoints = json_insert.getString("insert_sql_DayGameCoinPoints").replace("'?'", times);
        String insert_sql_DayCoinUser= json_insert.getString("insert_sql_DayCoinUser").replace("'?'", times);
        String insert_sql_DayGameCoinUser = json_insert.getString("insert_sql_DayGameCoinUser").replace("'?'", times);
        String insert_sql_DayCoinArppu = json_insert.getString("insert_sql_DayCoinArppu").replace("'?'", times);
        String insert_sql_MonCoinRevenuue = json_insert.getString("insert_sql_MonCoinRevenuue").replace("'?'", times);
        String insert_sql_MonCoinPoints = json_insert.getString("insert_sql_MonCoinPoints").replace("'?'", times);
        String insert_sql_MonCoinUser = json_insert.getString("insert_sql_MonCoinUser").replace("'?'", times);
        String insert_sql_MonCoinArppu= json_insert.getString("insert_sql_MonCoinArppu").replace("'?'", times);
        String insert_sql_DayCashRevenue= json_insert.getString("insert_sql_DayCashRevenue").replace("'?'", times);
        String insert_sql_DayCashPoints = json_insert.getString("insert_sql_DayCashPoints").replace("'?'", times);
        String insert_sql_DayCashUser = json_insert.getString("insert_sql_DayCashUser").replace("'?'", times);
        String insert_sql_DayCashArppu = json_insert.getString("insert_sql_DayCashArppu").replace("'?'", times);
        String insert_sql_MonCashRevenue = json_insert.getString("insert_sql_MonCashRevenue").replace("'?'", times);
        String insert_sql_MonCashPoints = json_insert.getString("insert_sql_MonCashPoints").replace("'?'", times);
        String insert_sql_MonCashUser = json_insert.getString("insert_sql_MonCashUser").replace("'?'", times);
        String insert_sql_MonCashArppu = json_insert.getString("insert_sql_MonCashArppu").replace("'?'", times);
        sqllist_insert.add(insert_sql_dau);
        sqllist_insert.add(insert_sql_nru);
        sqllist_insert.add(insert_sql_bnsdau);
        sqllist_insert.add(insert_sql_das);
        sqllist_insert.add(insert_sql_diu);
        sqllist_insert.add(insert_sql_30ltv);
        sqllist_insert.add(insert_sql_FirstRecharge);
        sqllist_insert.add(insert_sql_d1);
        sqllist_insert.add(insert_sql_d3);
        sqllist_insert.add(insert_sql_d7);
        sqllist_insert.add(insert_sql_d15);
        sqllist_insert.add(insert_sql_d30);
        sqllist_insert.add(insert_sql_DayCoinRevenuue);
        sqllist_insert.add(insert_sql_DayGameCoinRevenuue);
        sqllist_insert.add(insert_sql_DayCoinPoints);
        sqllist_insert.add(insert_sql_DayGameCoinPoints);
        sqllist_insert.add(insert_sql_DayCoinUser);
        sqllist_insert.add(insert_sql_DayGameCoinUser);
        sqllist_insert.add(insert_sql_DayCoinArppu);
        sqllist_insert.add(insert_sql_MonCoinRevenuue);
        sqllist_insert.add(insert_sql_MonCoinPoints);
        sqllist_insert.add(insert_sql_MonCoinUser);
        sqllist_insert.add(insert_sql_MonCoinArppu);
        sqllist_insert.add(insert_sql_DayCashRevenue);
        sqllist_insert.add(insert_sql_DayCashPoints);
        sqllist_insert.add(insert_sql_DayCashUser);
        sqllist_insert.add(insert_sql_DayCashArppu);
        sqllist_insert.add(insert_sql_MonCashRevenue);
        sqllist_insert.add(insert_sql_MonCashPoints);
        sqllist_insert.add(insert_sql_MonCashUser);
        sqllist_insert.add(insert_sql_MonCashArppu);
        if(flag.equals("delete")){
            runData(sqllist);
        }else {
            boolean b = runData(sqllist_insert);
            System.out.println(b);
        }

    }

    //读取json文件
    public static String readJsonFile(String fileName) {
        String jsonStr = "";
        try {
            File jsonFile = new File(fileName);
            FileReader fileReader = new FileReader(jsonFile);
            Reader reader = new InputStreamReader(new FileInputStream(jsonFile),"utf-8");
            int ch = 0;
            StringBuffer sb = new StringBuffer();
            while ((ch = reader.read()) != -1) {
                sb.append((char) ch);
            }
            fileReader.close();
            reader.close();
            jsonStr = sb.toString();
            return jsonStr;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }



    //执行SQL语句
    private static boolean runData(List<String> runSqls) {
        Connection connection = null;
        try {
            connection = RedShift_JDBCUtil.getConnection();
            for (String sql : runSqls) {
                Statement statement = connection.createStatement();
                statement.execute(sql);
                statement.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        } finally {
            try {
                if (connection == null) {
                    connection.close();
                }
            } catch (Exception e) {
            }
        }
        return true;
    }


}
