import util.RedShift_JDBCUtil;

import java.sql.Connection;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class DwdtoDws {
    public static void main(String[] args) {
        System.out.println("start computer --> ");
        boolean result = runData();
        System.out.println("task is successful");
        System.out.println("result----->" + result);
    }


    private static boolean runData(){
        List<String> runSqls = getRunSql_dnb();
        Connection connection = null;
        try {
            connection = RedShift_JDBCUtil.getConnection();
            for (String sql : runSqls) {
                Statement statement = connection.createStatement();
                statement.execute(sql);
                statement.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        } finally {
            try {
                if (connection == null) {
                    connection.close();
                }
            } catch (Exception e) {
            }
        }
        return true;
    }



    private static List<String> getjsonsql(){
        List<String> sqls = new ArrayList<String>();


        return sqls;
    }

    private static List<String> getRunSql_dnb() {


        List<String> sqls = new ArrayList<String>();
        sqls.add("DELETE " +
                "FROM bi_dws.dws_wide_index_report_d " +
                "WHERE date = date(CURRENT_DATE - interval '1 day')");



        sqls.add("INSERT INTO bi_dws.dws_wide_index_report_d " +
                "( " +
                "SELECT date,region, " +
                "SUM(CASE WHEN index_desc = 'DAU' THEN cast(RESULT as int) ELSE 0 END) as \"DAU\", " +
                "SUM(CASE WHEN index_desc = 'NRU' THEN cast(RESULT as int) ELSE 0 END) as \"NRU\", " +
                "SUM(CASE WHEN index_desc = 'BusinessNRU' THEN cast(RESULT as int) ELSE 0 END) as \"BusinessNRU\", " +
                "SUM(CASE WHEN index_desc = 'DAS' THEN cast(RESULT as int) ELSE 0 END) as \"DAS\", " +
                "SUM(CASE WHEN index_desc = 'DIU' THEN cast(RESULT as int) ELSE 0 END) as \"DIU\", " +
                "SUM(CASE WHEN index_desc = '30LTV' THEN cast(RESULT as decimal(10,2)) ELSE 0 END) as \"LTV30\", " +
                "SUM(CASE WHEN index_desc = 'FirstRecharge' THEN cast(RESULT as int) ELSE 0 END) as \"FirstRecharge\", " +
                "SUM(CASE WHEN index_desc = 'D1' THEN cast(RESULT as int) ELSE 0 END) as \"D1\", " +
                "SUM(CASE WHEN index_desc = 'D3' THEN cast(RESULT as int) ELSE 0 END) as \"D3\", " +
                "SUM(CASE WHEN index_desc = 'D7' THEN cast(RESULT as int) ELSE 0 END) as \"D7\", " +
                "SUM(CASE WHEN index_desc = 'D15' THEN cast(RESULT as int) ELSE 0 END) as \"D15\", " +
                "SUM(CASE WHEN index_desc = 'D30' THEN cast(RESULT as int) ELSE 0 END) as \"D30\", " +
                "SUM(CASE WHEN index_desc = 'DayCoinRevenuue' THEN cast(RESULT as int) ELSE 0 END) as \"DayCoinRevenuue\", " +
                "SUM(CASE WHEN index_desc = 'DayGameCoinRevenuue' THEN cast(RESULT as int) ELSE 0 END) as \"DayGameCoinRevenuue\", " +
                "SUM(CASE WHEN index_desc = 'DayCoinPoints' THEN cast(RESULT as int) ELSE 0 END) as \"DayCoinPoints\", " +
                "SUM(CASE WHEN index_desc = 'DayGameCoinPoints' THEN cast(RESULT as int) ELSE 0 END) as \"DayGameCoinPoints\", " +
                "SUM(CASE WHEN index_desc = 'DayCoinUser' THEN cast(RESULT as int) ELSE 0 END) as \"DayCoinUser\", " +
                "SUM(CASE WHEN index_desc = 'DayGameCoinUser' THEN cast(RESULT as int) ELSE 0 END) as \"DayGameCoinUser\", " +
                "SUM(CASE WHEN index_desc = 'DayCoinArppu' THEN cast(RESULT as int) ELSE 0 END) as \"DayCoinArppu\", " +
                "SUM(CASE WHEN index_desc = 'MonCoinRevenue' THEN cast(RESULT as int) ELSE 0 END) as \"MonCoinRevenue\", " +
                "SUM(CASE WHEN index_desc = 'MonCoinPoints' THEN cast(RESULT as int) ELSE 0 END) as \"MonCoinPoints\", " +
                "SUM(CASE WHEN index_desc = 'MonCoinUser' THEN cast(RESULT as int) ELSE 0 END) as \"MonCoinUser\", " +
                "SUM(CASE WHEN index_desc = 'MonCoinArppu' THEN cast(RESULT as int) ELSE 0 END) as \"MonCoinArppu\", " +
                "SUM(CASE WHEN index_desc = 'DayCashRevenue' THEN cast(RESULT as int) ELSE 0 END) as \"DayCashRevenue\", " +
                "SUM(CASE WHEN index_desc = 'DayCashPoints' THEN cast(RESULT as int) ELSE 0 END) as \"DayCashPoints\", " +
                "SUM(CASE WHEN index_desc = 'DayCashUser' THEN cast(RESULT as int) ELSE 0 END) as \"DayCashUser\", " +
                "SUM(CASE WHEN index_desc = 'DayCashArppu' THEN cast(RESULT as int) ELSE 0 END) as \"DayCashArppu\", " +
                "SUM(CASE WHEN index_desc = 'MonCashRevenuue' THEN cast(RESULT as int) ELSE 0 END) as \"MonCashRevenuue\", " +
                "SUM(CASE WHEN index_desc = 'MonCashPoints' THEN cast(RESULT as int) ELSE 0 END) as \"MonCashPoints\", " +
                "SUM(CASE WHEN index_desc = 'MonCashUser' THEN cast(RESULT as int) ELSE 0 END) as \"MonCashUser\", " +
                "SUM(CASE WHEN index_desc = 'MonCashArppu' THEN cast(RESULT as int) ELSE 0 END) as \"MonCashArppu\" " +
                "FROM bi_dwd.dwd_wide_index_source_d where date = date(CURRENT_DATE - interval '1 day') " +
                "group by 1,2 " +
                ");");

        return sqls;
    }
}
