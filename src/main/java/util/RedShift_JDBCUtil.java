package util;

import com.amazon.redshift.jdbc.DataSource;

import java.sql.*;
import java.util.Properties;

public class RedShift_JDBCUtil {
    public static String driver;
    public static String url;
    public static String user;
    public static String password;

    static {
        try {
            Properties pro = new Properties();
            //jdbc.properties一般是放在src目录下的配置文件具体内容下面给出
            pro.load(RedShift_JDBCUtil.class.getClassLoader().getResourceAsStream("data.properties"));
            url = pro.getProperty("redshift_url");
            user = pro.getProperty("redshift_user");
            password = pro.getProperty("redshift_password");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //获取Connection对象
    public static Connection getConnection() throws Exception {
        DataSource dataSource = new com.amazon.redshift.jdbc42.DataSource();
        dataSource.setURL(url);
        return dataSource.getConnection(user, password);
    }


    //关闭资源
    public static void close(PreparedStatement pstat, Connection conn){
        if(pstat!=null){
            try {
                pstat.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        if(conn!=null){
            try {
                conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public static void close(ResultSet rs, PreparedStatement pstat, Connection conn){
        if(rs!=null){
            try {
                rs.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        close(pstat,conn);
    }
}
