package util;

import util.Mysql_JDBCUtil;
import util.RedShift_JDBCUtil;

import java.sql.*;

public class MysqlUtil {

    static PreparedStatement preparedStatement =null;
    static Connection connection =null;
    static Connection redshift_connection =null;

    public static void main(String[] args) throws Exception {

       // delete_to_mysql("delete from test_student where dt=?");
        update_to_mysql("update test_student set name=? where dt=?");



//        System.out.println("====mysql student===");
//        ResultSet resultSets = query_redshift();
//        System.out.println("====redshift student===");
//        redshift_to_mysql(resultSets);
    }


    /**
     * 从redshift查询出数据
     * @return
     * @throws Exception
     */
    public static ResultSet query_redshift() throws Exception {
        redshift_connection = RedShift_JDBCUtil.getConnection();
        String sql = "select * from meme_dw.dw_basis_dnb_test";
        preparedStatement = connection.prepareStatement(sql);
        return preparedStatement.executeQuery(sql);
    }


    /**
     *
     * 将redshift查询出的数据插入到mysql
     * @param mysql_result
     * @throws Exception
     */
    public static void redshift_to_mysql(ResultSet mysql_result) throws Exception {
        //获取connection对象
        connection = Mysql_JDBCUtil.getConnection();

        //拼接变量
        StringBuilder columnNames = new StringBuilder();
        StringBuilder bindVariables = new StringBuilder();
        //获取字段
        ResultSetMetaData meta = mysql_result.getMetaData();
        for (int i = 1; i <= meta.getColumnCount(); i++){
            if (i > 1) {
                columnNames.append(", ");
                bindVariables.append(", ");
            }
            columnNames.append(meta.getColumnName(i));
            bindVariables.append('?');
        }

        //SQL处理--》插入
        String sql = "INSERT INTO test_student"  + " ("
                + columnNames
                + ") VALUES ("
                + bindVariables
                + ")";

        System.out.println(sql);

        preparedStatement = connection.prepareStatement(sql);
        while (mysql_result.next()) {
            for (int i = 1; i <= meta.getColumnCount(); i++)
                preparedStatement.setObject(i, mysql_result.getObject(i));
            preparedStatement.addBatch();
        }
        preparedStatement.executeBatch();
        redshift_connection.close();
        Mysql_JDBCUtil.close(mysql_result,preparedStatement, connection);
    }

    public static void delete_to_mysql(String sql) throws Exception{
        //获取connection对象
        connection = Mysql_JDBCUtil.getConnection();
        //String sql_delete="delete from test_student where dt=?";
        preparedStatement=connection.prepareStatement(sql);
        preparedStatement.setObject(1,"2021-01-29 13:53:17");
        int i = preparedStatement.executeUpdate();
        System.out.println(i);
        Mysql_JDBCUtil.close(preparedStatement, connection);
    }

    /**
     * 修改数据
     * @param sql
     * @throws Exception
     */
    public static void update_to_mysql(String sql) throws Exception{
        //获取connection对象
        connection = Mysql_JDBCUtil.getConnection();
        preparedStatement=connection.prepareStatement(sql);
        preparedStatement.setObject(1,"lisi12312312");
        preparedStatement.setObject(2,"2021-01-26 17:53:17");
        int i = preparedStatement.executeUpdate();
        System.out.println(i);
        Mysql_JDBCUtil.close(preparedStatement, connection);
    }






}