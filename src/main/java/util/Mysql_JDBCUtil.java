package util;

import java.io.IOException;
import java.sql.*;
import java.util.Properties;

public class Mysql_JDBCUtil {

    public static String driver;
    public static String url;
    public static String user;
    public static String password;

    static {
        try {
            Properties pro = new Properties();
            //jdbc.properties一般是放在src目录下的配置文件具体内容下面给出
            pro.load(Mysql_JDBCUtil.class.getClassLoader().getResourceAsStream("data.properties"));
            driver = pro.getProperty("jdbc_driver");
            url = pro.getProperty("jdbc_url");
            user = pro.getProperty("jdbc_user");
            password = pro.getProperty("jdbc_password");
            Class.forName(driver);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
    //获取Connection对象
    public static Connection getConnection() throws SQLException {
        return DriverManager.getConnection(url,user,password);
    }
    //关闭资源
    public static void close(PreparedStatement pstat, Connection conn){
        if(pstat!=null){
            try {
                pstat.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        if(conn!=null){
            try {
                conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public static void close(ResultSet rs, PreparedStatement pstat, Connection conn){
        if(rs!=null){
            try {
                rs.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        close(pstat,conn);
    }


}
