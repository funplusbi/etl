import util.RedShift_JDBCUtil;

import java.sql.Connection;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class OdstoDwd {

    public static void main(String[] args) {
        System.out.println("start computer --> ");
        boolean result = runData();
        System.out.println("task is successful");
        System.out.println("result----->" + result);
    }


    private static boolean runData() {
        List<String> runSqls = getRunSql_dnb();
        Connection connection = null;
        try {
            connection = RedShift_JDBCUtil.getConnection();
            for (String sql : runSqls) {
                Statement statement = connection.createStatement();
                statement.execute(sql);
                statement.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        } finally {
            try {
                if (connection == null) {
                    connection.close();
                }
            } catch (Exception e) {
            }
        }
        return true;
    }


    private static List<String> getRunSql_dnb() {
        List<String> sqls = new ArrayList<String>();
        //dau
        sqls.add("DELETE " +
                "FROM bi_dwd.dwd_wide_index_source_d " +
                "WHERE date = date(CURRENT_DATE - interval '1 day')");
        sqls.add("INSERT INTO bi_dwd.dwd_wide_index_source_d " +
                "(SELECT date(ts) AS date, region6 AS region, 'DAU' AS index_desc, count(DISTINCT device_id) AS RESULT " +
                "FROM meme_dw.flow_source_dau_result " +
                "WHERE date(ts) = date(CURRENT_DATE - interval '1 day') " +
                "GROUP BY 1,2)");
        //nru
        sqls.add("DELETE " +
                "FROM bi_dwd.dwd_wide_index_source_d " +
                "WHERE date = date(CURRENT_DATE - interval '1 day') " +
                "and index_desc = 'NRU'");

        sqls.add("INSERT INTO bi_dwd.dwd_wide_index_source_d " +
                "(SELECT date(ts) AS date, region6 AS region, 'NRU' AS index_desc, count(DISTINCT device_id) AS RESULT " +
                "FROM meme_dw.flow_source_dau_result " +
                "WHERE date(ts) = date(CURRENT_DATE - interval '1 day') " +
                "and nru = 1 " +
                "GROUP BY 1,2)");
        //bus_nru
        sqls.add("DELETE " +
                "FROM bi_dwd.dwd_wide_index_source_d " +
                "WHERE date = date(CURRENT_DATE - interval '1 day') " +
                "and index_desc = 'BusinessNRU'");
        sqls.add("INSERT INTO bi_dwd.dwd_wide_index_source_d " +
                "(SELECT date(ts) AS date, region6 AS region, 'BusinessNRU' AS index_desc, count(DISTINCT device_id) AS RESULT " +
                "FROM meme_dw.flow_source_dau_result " +
                "WHERE date(ts) = date(CURRENT_DATE - interval '1 day') " +
                "and nru=1 and (install_source " +
                "not in ('主播下载页（客户端分享FB）','Adjust Organic','主播播放页','超人气主播投放KPI') " +
                "and install_source not like '%rganic%' and install_source is not null and fpid is not null) " +
                "GROUP BY 1,2)");

        //das
        sqls.add("DELETE " +
                "FROM bi_dwd.dwd_wide_index_source_d " +
                "WHERE date = date(CURRENT_DATE - interval '1 day') " +
                "and index_desc = 'DAS'");
        sqls.add("INSERT INTO bi_dwd.dwd_wide_index_source_d  " +
                "(  " +
                "select b.date,a.region,'DAS' AS index_desc,count(distinct a.uid) as RESULT  " +
                "from  " +
                "(  " +
                "select  " +
                "distinct  " +
                "uid,  " +
                "case  " +
                "when country in('PERKY','JP') then 'JP'  " +
                "when country in('CN',' CN') then 'CN'  " +
                "when country in (' TW','TW','SG','CA','AU','US','MY','HK','MO','KR') then 'TW'  " +
                "when country in('IN',' PK') then 'IN'  " +
                "else 'mean' end region  " +
                "from fpsync.fp_user  " +
                ")a  " +
                "join  " +
                "(  " +
                "select uid ,date(ctime) as date , sum(datediff('second',ctime,endtime) )  " +
                "from fpsync.fp_live_creator  " +
                "where date(ctime) = date(CURRENT_DATE - interval '1 day')  " +
                "group by 1,2  " +
                "having sum(datediff('second',ctime,endtime) ) > 1800  " +
                ")b  " +
                "on a.uid = b.uid  " +
                "group by 1,2  " +
                ")");

        //diu
        sqls.add("DELETE " +
                "FROM bi_dwd.dwd_wide_index_source_d " +
                "WHERE date = date(CURRENT_DATE - interval '1 day') " +
                "and index_desc = 'DIU' ");

        sqls.add("INSERT INTO bi_dwd.dwd_wide_index_source_d  " +
                "(  " +
                "select b.dt ,a.region,'DIU' AS index_desc,count(distinct a.uid) as RESULT  " +
                "from  " +
                "(  " +
                "select  " +
                "distinct  " +
                "uid,  " +
                "case  " +
                "when country in('PERKY','JP') then 'JP'  " +
                "when country in('CN',' CN') then 'CN'  " +
                "when country in (' TW','TW','SG','CA','AU','US','MY','HK','MO','KR') then 'TW'  " +
                "when country in('IN',' PK') then 'IN'  " +
                "else 'mean' end region  " +
                "from fpsync.fp_user  " +
                ")a  " +
                "join  " +
                "(  " +
                "select uid ,dt,substring(dt,1,7) mon from  " +
                "(  " +
                "  " +
                "select date(dt) dt,uid from work.custom_events where  " +
                "event_name='live_room_chat'  " +
                "and date(dt) = date(CURRENT_DATE - interval '1 day')  " +
                "  " +
                "union  " +
                "  " +
                "select  " +
                "  " +
                "date(mtime) as dt,  " +
                "fromuid as uid  " +
                "from fpsync.fp_receive_gift  " +
                "where date(mtime)=date(CURRENT_DATE - interval '1 day')  " +
                "  " +
                "union  " +
                "  " +
                "select  " +
                "date(mtime) as dt,  " +
                "uid  " +
                "from fpsync.fp_friends  " +
                "where date(mtime)=date(CURRENT_DATE - interval '1 day')  " +
                ")bb  " +
                ") as b  " +
                "on a.uid = b.uid  " +
                "group by 1,2  " +
                ")");


        //30ltv
        sqls.add("DELETE " +
                "FROM bi_dwd.dwd_wide_index_source_d " +
                "WHERE date = date(CURRENT_DATE - interval '1 day') " +
                "and index_desc = '30LTV' ");
        sqls.add("INSERT INTO bi_dwd.dwd_wide_index_source_d  " +
                "(  " +
                "select  " +
                "a.dt,  " +
                "a.region,  " +
                "'30LTV' AS index_desc,  " +
                "sum(usd)/count(distinct a.uid) as RESULT  " +
                "from  " +
                "(  " +
                "select  " +
                "distinct  " +
                "region6 as region,  " +
                "date(current_date - interval '1 day') as dt,  " +
                "uid  " +
                "FROM meme_dw.flow_source_dau_result  " +
                "where nru = 1  " +
                "and date(ts) = date(current_date - interval '30 day')  " +
                ") as a  " +
                "left join  " +
                "(  " +
                "select  " +
                "userid as uid,  " +
                "sum(usd) as usd  " +
                "from  " +
                "work.fifo_revenue  " +
                "where day between date(current_date - interval '30 day') and date(current_date - interval '1 day')  " +
                "group by 1  " +
                ") as b  " +
                "on a.uid = b.uid  " +
                "group by 1,2  " +
                ") ");


        //首充人数
        sqls.add("DELETE " +
                "FROM bi_dwd.dwd_wide_index_source_d " +
                "WHERE date = date(CURRENT_DATE - interval '1 day') " +
                "and index_desc = 'FirstRecharge'");

        sqls.add("INSERT INTO bi_dwd.dwd_wide_index_source_d  " +
                "(  " +
                "select  " +
                "a.first_des,  " +
                "b.region,  " +
                "'FirstRecharge' AS index_desc,  " +
                "count(distinct a.uid) as RESULT  " +
                "from  " +
                "(  " +
                "select  " +
                "uid,  " +
                "min(date(ts)) as first_des  " +
                "from work.funbank_meme_deposit  " +
                "group by 1  " +
                "having first_des = date(CURRENT_DATE - interval '1 day')  " +
                ") as a  " +
                "join  " +
                "(  " +
                "select  " +
                "distinct  " +
                "uid,  " +
                "case  " +
                "when country in('PERKY','JP') then 'JP'  " +
                "when country in('CN',' CN') then 'CN'  " +
                "when country in (' TW','TW','SG','CA','AU','US','MY','HK','MO','KR') then 'TW'  " +
                "when country in('IN',' PK') then 'IN'  " +
                "else 'mean' end region  " +
                "from fpsync.fp_user  " +
                ") as b  " +
                "on a.uid = b.uid  " +
                "group by 1,2  " +
                ")");


        //留存数据

        //1日留存
        sqls.add("DELETE " +
                "FROM bi_dwd.dwd_wide_index_source_d " +
                "WHERE date = date(CURRENT_DATE - interval '1 day') " +
                "and index_desc = 'D1' ");

        sqls.add("INSERT INTO bi_dwd.dwd_wide_index_source_d  " +
                "(  " +
                "select  " +
                "dt,  " +
                "region,  " +
                "'D1' AS index_desc,  " +
                "round(d1*100.00/nru,2) as RESULT  " +
                "from  " +
                "(  " +
                "select  " +
                "a.region,  " +
                "date(a.dt + interval '1 day') as dt,  " +
                "count(distinct a.device_id) as nru,  " +
                "count(distinct b.device_id) as d1  " +
                "from  " +
                "(  " +
                "select  " +
                "date(ts) as dt,  " +
                "region6 as region,  " +
                "device_id  " +
                "FROM meme_dw.flow_source_dau_result  " +
                "where nru = 1  " +
                "and date(ts) = date(CURRENT_DATE - interval '2 day')  " +
                ") as a  " +
                "left join  " +
                "(  " +
                "select  " +
                "date(ts) as dt,  " +
                "device_id  " +
                "FROM meme_dw.flow_source_dau_result  " +
                "where date(ts) = date(CURRENT_DATE - interval '1 day')  " +
                ") as b  " +
                "on a.device_id = b.device_id  " +
                "group by 1,2  " +
                ") as w  " +
                ") ");

//3日留存
        sqls.add("DELETE " +
                "FROM bi_dwd.dwd_wide_index_source_d " +
                "WHERE date = date(CURRENT_DATE - interval '1 day') " +
                "and index_desc = 'D3' ");

        sqls.add("INSERT INTO bi_dwd.dwd_wide_index_source_d  " +
                "(  " +
                "select  " +
                "dt,  " +
                "region,  " +
                "'D3' AS index_desc,  " +
                "round(d3*100.00/nru,2) as RESULT  " +
                "from  " +
                "(  " +
                "select  " +
                "a.region,  " +
                "date(a.dt + interval '3 day') as dt,  " +
                "count(distinct a.device_id) as nru,  " +
                "count(distinct b.device_id) as d3  " +
                "from  " +
                "(  " +
                "select  " +
                "date(ts) as dt,  " +
                "region6 as region,  " +
                "device_id  " +
                "FROM meme_dw.flow_source_dau_result  " +
                "where nru = 1  " +
                "and date(ts) = date(CURRENT_DATE - interval '4 day')  " +
                ") as a  " +
                "left join  " +
                "(  " +
                "select  " +
                "date(ts) as dt,  " +
                "device_id  " +
                "FROM meme_dw.flow_source_dau_result  " +
                "where date(ts) = date(CURRENT_DATE - interval '1 day')  " +
                ") as b  " +
                "on a.device_id = b.device_id  " +
                "group by 1,2  " +
                ") as w  " +
                ") ");

//7日留存
        sqls.add("DELETE " +
                "FROM bi_dwd.dwd_wide_index_source_d " +
                "WHERE date = date(CURRENT_DATE - interval '1 day') " +
                "and index_desc = 'D7' ");

        sqls.add("INSERT INTO bi_dwd.dwd_wide_index_source_d  " +
                "(  " +
                "select  " +
                "dt,  " +
                "region,  " +
                "'D7' AS index_desc,  " +
                "round(d7*100.00/nru,2) as RESULT  " +
                "from  " +
                "(  " +
                "select  " +
                "a.region,  " +
                "date(a.dt + interval '7 day') as dt,  " +
                "count(distinct a.device_id) as nru,  " +
                "count(distinct b.device_id) as d7  " +
                "from  " +
                "(  " +
                "select  " +
                "date(ts) as dt,  " +
                "region6 as region,  " +
                "device_id  " +
                "FROM meme_dw.flow_source_dau_result  " +
                "where nru = 1  " +
                "and date(ts) = date(CURRENT_DATE - interval '8 day')  " +
                ") as a  " +
                "left join  " +
                "(  " +
                "select  " +
                "date(ts) as dt,  " +
                "device_id  " +
                "FROM meme_dw.flow_source_dau_result  " +
                "where date(ts) = date(CURRENT_DATE - interval '1 day')  " +
                ") as b  " +
                "on a.device_id = b.device_id  " +
                "group by 1,2  " +
                ") as w  " +
                ") ");

//15日留存
        sqls.add("DELETE " +
                "FROM bi_dwd.dwd_wide_index_source_d " +
                "WHERE date = date(CURRENT_DATE - interval '1 day') " +
                "and index_desc = 'D15' ");

        sqls.add("INSERT INTO bi_dwd.dwd_wide_index_source_d  " +
                "(  " +
                "select  " +
                "dt,  " +
                "region,  " +
                "'D15' AS index_desc,  " +
                "round(d15*100.00/nru,2) as RESULT  " +
                "from  " +
                "(  " +
                "select  " +
                "a.region,  " +
                "date(a.dt + interval '15 day') as dt,  " +
                "count(distinct a.device_id) as nru,  " +
                "count(distinct b.device_id) as d15  " +
                "from  " +
                "(  " +
                "select  " +
                "date(ts) as dt,  " +
                "region6 as region,  " +
                "device_id  " +
                "FROM meme_dw.flow_source_dau_result  " +
                "where nru = 1  " +
                "and date(ts) = date(CURRENT_DATE - interval '16 day')  " +
                ") as a  " +
                "left join  " +
                "(  " +
                "select  " +
                "date(ts) as dt,  " +
                "device_id  " +
                "FROM meme_dw.flow_source_dau_result  " +
                "where date(ts) = date(CURRENT_DATE - interval '1 day')  " +
                ") as b  " +
                "on a.device_id = b.device_id  " +
                "group by 1,2  " +
                ") as w  " +
                ") ");

//30日留存
        sqls.add("DELETE " +
                "FROM bi_dwd.dwd_wide_index_source_d " +
                "WHERE date = date(CURRENT_DATE - interval '1 day') " +
                "and index_desc = 'D30' ");

        sqls.add("INSERT INTO bi_dwd.dwd_wide_index_source_d " +
                "( " +
                "select " +
                "dt, " +
                "region, " +
                "'D30' AS index_desc, " +
                "round(d30*100.00/nru,2) as RESULT " +
                "from " +
                "( " +
                "select " +
                "a.region, " +
                "date(a.dt + interval '30 day') as dt, " +
                "count(distinct a.device_id) as nru, " +
                "count(distinct b.device_id) as d30 " +
                "from " +
                "( " +
                "select " +
                "date(ts) as dt, " +
                "region6 as region, " +
                "device_id " +
                "FROM meme_dw.flow_source_dau_result " +
                "where nru = 1 " +
                "and date(ts) = date(CURRENT_DATE - interval '31 day') " +
                ") as a " +
                "left join " +
                "( " +
                "select " +
                "date(ts) as dt, " +
                "device_id " +
                "FROM meme_dw.flow_source_dau_result " +
                "where date(ts) = date(CURRENT_DATE - interval '1 day') " +
                ") as b " +
                "on a.device_id = b.device_id " +
                "group by 1,2 " +
                ") as w " +
                ") ");


        //确认收入

        sqls.add("DELETE " +
                "FROM bi_dwd.dwd_wide_index_source_d " +
                "WHERE date = date(CURRENT_DATE - interval '1 day') " +
                "and index_desc = 'DayCoinRevenuue' ");


        sqls.add("INSERT INTO bi_dwd.dwd_wide_index_source_d " +
                "( " +
                "select " +
                "a.day, " +
                "c.region, " +
                "'DayCoinRevenuue' AS index_desc, " +
                "sum(usd) as RESULT " +
                "from " +
                "( " +
                "select " +
                "* " +
                "from " +
                "work.fifo_revenue " +
                "where day = date(CURRENT_DATE - interval '1 day') " +
                ") as a " +
                "join " +
                "( " +
                "select " +
                "distinct " +
                "uid, " +
                "country " +
                "from fpsync.fp_user " +
                ")as b " +
                "on a.userid = b.uid " +
                "left join " +
                "( " +
                "select distinct country,region1 as region from meme_dw.dim_country " +
                ")as c " +
                "on b.country = c.country " +
                "group by 1,2) ");

        sqls.add("DELETE " +
                "FROM bi_dwd.dwd_wide_index_source_d " +
                "WHERE date = date(CURRENT_DATE - interval '1 day') " +
                "and index_desc = 'DayGameCoinRevenuue' ");
        sqls.add("INSERT INTO bi_dwd.dwd_wide_index_source_d " +
                "( " +
                "select " +
                "a.day, " +
                "c.region, " +
                "'DayGameCoinRevenuue' AS index_desc, " +
                "sum(usd) as RESULT " +
                "from " +
                "( " +
                "select " +
                "* " +
                "from " +
                "work.fifo_revenue " +
                "where day = date(CURRENT_DATE - interval '1 day') " +
                "and type in ('46','54','53','58','60','64','73','74','76') " +
                ") as a " +
                "join " +
                "( " +
                "select " +
                "distinct " +
                "uid, " +
                "country " +
                "from fpsync.fp_user " +
                ")as b " +
                "on a.userid = b.uid " +
                "left join " +
                "( " +
                "select distinct country,region1 as region from meme_dw.dim_country " +
                ")as c " +
                "on b.country = c.country " +
                "group by 1,2) ");

        sqls.add("DELETE " +
                "FROM bi_dwd.dwd_wide_index_source_d " +
                "WHERE date = date(CURRENT_DATE - interval '1 day') " +
                "and index_desc = 'DayCoinPoints' ");

        sqls.add("INSERT INTO bi_dwd.dwd_wide_index_source_d " +
                "( " +
                "select " +
                "a.day, " +
                "c.region, " +
                "'DayCoinPoints' AS index_desc, " +
                "sum(amount) as RESULT " +
                "from " +
                "( " +
                "select " +
                "* " +
                "from " +
                "work.fifo_revenue " +
                "where day = date(CURRENT_DATE - interval '1 day') " +
                ") as a " +
                "join " +
                "( " +
                "select " +
                "distinct " +
                "uid, " +
                "country " +
                "from fpsync.fp_user " +
                ")as b " +
                "on a.userid = b.uid " +
                "left join " +
                "( " +
                "select distinct country,region1 as region from meme_dw.dim_country " +
                ")as c " +
                "on b.country = c.country " +
                "group by 1,2) ");


        sqls.add("DELETE " +
                "FROM bi_dwd.dwd_wide_index_source_d " +
                "WHERE date = date(CURRENT_DATE - interval '1 day') " +
                "and index_desc = 'DayGameCoinPoints' ");

        sqls.add("INSERT INTO bi_dwd.dwd_wide_index_source_d " +
                "( " +
                "select " +
                "a.day, " +
                "c.region, " +
                "'DayGameCoinPoints' AS index_desc, " +
                "sum(amount) as RESULT " +
                "from " +
                "( " +
                "select " +
                "* " +
                "from " +
                "work.fifo_revenue " +
                "where day = date(CURRENT_DATE - interval '1 day') " +
                "and type in ('46','54','53','58','60','64','73','74','76') " +
                ") as a " +
                "join " +
                "( " +
                "select " +
                "distinct " +
                "uid, " +
                "country " +
                "from fpsync.fp_user " +
                ")as b " +
                "on a.userid = b.uid " +
                "left join " +
                "( " +
                "select distinct country,region1 as region from meme_dw.dim_country " +
                ")as c " +
                "on b.country = c.country " +
                "group by 1,2) ");

        sqls.add("DELETE " +
                "FROM bi_dwd.dwd_wide_index_source_d " +
                "WHERE date = date(CURRENT_DATE - interval '1 day') " +
                "and index_desc = 'DayCoinUser' ");
        sqls.add("INSERT INTO bi_dwd.dwd_wide_index_source_d " +
                "( " +
                "select " +
                "a.day, " +
                "c.region, " +
                "'DayCoinUser' AS index_desc, " +
                "count(distinct userid) as RESULT " +
                "from " +
                "( " +
                "select " +
                "* " +
                "from " +
                "work.fifo_revenue " +
                "where day = date(CURRENT_DATE - interval '1 day') " +
                ") as a " +
                "join " +
                "( " +
                "select " +
                "distinct " +
                "uid, " +
                "country " +
                "from fpsync.fp_user " +
                ")as b " +
                "on a.userid = b.uid " +
                "left join " +
                "( " +
                "select distinct country,region1 as region from meme_dw.dim_country " +
                ")as c " +
                "on b.country = c.country " +
                "group by 1,2) ");


        sqls.add("DELETE " +
                "FROM bi_dwd.dwd_wide_index_source_d " +
                "WHERE date = date(CURRENT_DATE - interval '1 day') " +
                "and index_desc = 'DayGameCoinUser' ");

        sqls.add("INSERT INTO bi_dwd.dwd_wide_index_source_d " +
                "( " +
                "select " +
                "a.day, " +
                "c.region, " +
                "'DayGameCoinUser' AS index_desc, " +
                "count(distinct userid) as RESULT " +
                "from " +
                "( " +
                "select " +
                "* " +
                "from " +
                "work.fifo_revenue " +
                "where day = date(CURRENT_DATE - interval '1 day') " +
                "and type in ('46','54','53','58','60','64','73','74','76') " +
                ") as a " +
                "join " +
                "( " +
                "select " +
                "distinct " +
                "uid, " +
                "country " +
                "from fpsync.fp_user " +
                ")as b " +
                "on a.userid = b.uid " +
                "left join " +
                "( " +
                "select distinct country,region1 as region from meme_dw.dim_country " +
                ")as c " +
                "on b.country = c.country " +
                "group by 1,2) ");

        sqls.add("DELETE " +
                "FROM bi_dwd.dwd_wide_index_source_d " +
                "WHERE date = date(CURRENT_DATE - interval '1 day') " +
                "and index_desc = 'DayCoinArppu' ");

        sqls.add("INSERT INTO bi_dwd.dwd_wide_index_source_d " +
                "( " +
                "select " +
                "a.day, " +
                "c.region, " +
                "'DayCoinArppu' AS index_desc, " +
                "sum(usd)/count(distinct userid) as RESULT " +
                "from " +
                "( " +
                "select " +
                "* " +
                "from " +
                "work.fifo_revenue " +
                "where day = date(CURRENT_DATE - interval '1 day') " +
                ") as a " +
                "join " +
                "( " +
                "select " +
                "distinct " +
                "uid, " +
                "country " +
                "from fpsync.fp_user " +
                ")as b " +
                "on a.userid = b.uid " +
                "left join " +
                "( " +
                "select distinct country,region1 as region from meme_dw.dim_country " +
                ")as c " +
                "on b.country = c.country " +
                "group by 1,2)");


        sqls.add("DELETE " +
                "FROM bi_dwd.dwd_wide_index_source_d " +
                "WHERE date = date(CURRENT_DATE - interval '1 day') " +
                "and index_desc = 'DayCashRevenue' ");


        sqls.add("INSERT INTO bi_dwd.dwd_wide_index_source_d " +
                "( " +
                "select " +
                "date(ts) as date, " +
                "c.region , " +
                "'DayCashRevenue' AS index_desc, " +
                "sum(amount) as RESULT " +
                "from " +
                "work.funbank_meme_deposit a " +
                "join " +
                "( " +
                "select " +
                "distinct " +
                "uid, " +
                "country " +
                "from fpsync.fp_user " +
                ")as b " +
                "on a.uid = b.uid " +
                "left join " +
                "( " +
                "select distinct country,region1 as region from meme_dw.dim_country " +
                ")as c " +
                "on b.country = c.country " +
                "where date(ts) = date(CURRENT_DATE - interval '1 day') " +
                "group by 1,2 " +
                ") ");


        sqls.add("DELETE FROM bi_dwd.dwd_wide_index_source_d WHERE date = date(CURRENT_DATE - interval '1 day')" +
                "and index_desc = 'DayCashPoints'; ");

        sqls.add("INSERT INTO bi_dwd.dwd_wide_index_source_d " +
                "( " +
                "select " +
                "date(ts) as date, " +
                "c.region , " +
                "'DayCashPoints' AS index_desc, " +
                "sum( a.mcoin) as RESULT " +
                "from " +
                "work.funbank_meme_deposit a " +
                "join " +
                "( " +
                "select " +
                "distinct " +
                "uid, " +
                "country " +
                "from fpsync.fp_user " +
                ")as b " +
                "on a.uid = b.uid " +
                "left join " +
                "( " +
                "select distinct country,region1 as region from meme_dw.dim_country " +
                ")as c " +
                "on b.country = c.country " +
                "where date(ts) = date(CURRENT_DATE - interval '1 day') " +
                "group by 1,2 " +
                "); ");

        sqls.add("DELETE " +
                "FROM bi_dwd.dwd_wide_index_source_d " +
                "WHERE date = date(CURRENT_DATE - interval '1 day') " +
                "and index_desc = 'DayCashUser' ");
        sqls.add("INSERT INTO bi_dwd.dwd_wide_index_source_d " +
                "( " +
                "select " +
                "date(ts) as date, " +
                "c.region , " +
                "'DayCashUser' AS index_desc, " +
                "count(distinct a.uid) as RESULT " +
                "from " +
                "work.funbank_meme_deposit a " +
                "join " +
                "( " +
                "select " +
                "distinct " +
                "uid, " +
                "country " +
                "from fpsync.fp_user " +
                ")as b " +
                "on a.uid = b.uid " +
                "left join " +
                "( " +
                "select distinct country,region1 as region from meme_dw.dim_country " +
                ")as c " +
                "on b.country = c.country " +
                "where date(ts) = date(CURRENT_DATE - interval '1 day') " +
                "group by 1,2 " +
                ") ");


        sqls.add("DELETE " +
                "FROM bi_dwd.dwd_wide_index_source_d " +
                "WHERE date = date(CURRENT_DATE - interval '1 day') " +
                "and index_desc = 'DayCashArppu' ");


        sqls.add("INSERT INTO bi_dwd.dwd_wide_index_source_d " +
                "( " +
                "select " +
                "date(ts) as date, " +
                "c.region , " +
                "'DayCashArppu' AS index_desc, " +
                "sum(a.amount)/count(distinct a.uid) as RESULT " +
                "from " +
                "work.funbank_meme_deposit a " +
                "join " +
                "( " +
                "select " +
                "distinct " +
                "uid, " +
                "country " +
                "from fpsync.fp_user " +
                ")as b " +
                "on a.uid = b.uid " +
                "left join " +
                "( " +
                "select distinct country,region1 as region from meme_dw.dim_country " +
                ")as c " +
                "on b.country = c.country " +
                "where date(ts) = date(CURRENT_DATE - interval '1 day') " +
                "group by 1,2 " +
                ") ");


        sqls.add("DELETE " +
                "FROM meme_dw.dwd_wide_index_source_d " +
                "WHERE date = date(CURRENT_DATE - interval '1 day') " +
                "and index_desc = 'MonCashRevenue' ");

        sqls.add("INSERT INTO bi_dwd.dwd_wide_index_source_d " +
                "( " +
                "select " +
                "date(CURRENT_DATE - interval '1 day') as date, " +
                "region , " +
                "'MonCashRevenue' AS index_desc, " +
                "sum(amount) as RESULT " +
                "from " +
                "( " +
                "SELECT a.*, " +
                "c.region " +
                "FROM " +
                "(SELECT * " +
                "FROM work.funbank_meme_deposit " +
                "WHERE substring(date(ts),0,8) = substring(date(CURRENT_DATE - interval '1 day'),0,8)) a " +
                "JOIN fpsync.fp_user b ON a.uid = b.uid " +
                "LEFT JOIN " +
                "(SELECT DISTINCT country, " +
                "region1 AS region " +
                "FROM meme_dw.dim_country)AS c ON b.country = c.country) " +
                "group by 1,2) ");

        sqls.add("DELETE " +
                "FROM bi_dwd.dwd_wide_index_source_d " +
                "WHERE date = date(CURRENT_DATE - interval '1 day') " +
                "and index_desc = 'MonCashPoints' ");

        sqls.add("INSERT INTO bi_dwd.dwd_wide_index_source_d " +
                "( " +
                "select " +
                "date(CURRENT_DATE - interval '1 day') as date, " +
                "region , " +
                "'MonCashPoints' AS index_desc, " +
                "sum(mcoin) as RESULT " +
                "from " +
                "( " +
                "SELECT a.*, " +
                "c.region " +
                "FROM " +
                "(SELECT * " +
                "FROM work.funbank_meme_deposit " +
                "WHERE substring(date(ts),0,8) = substring(date(CURRENT_DATE - interval '1 day'),0,8)) a " +
                "JOIN fpsync.fp_user b ON a.uid = b.uid " +
                "LEFT JOIN " +
                "(SELECT DISTINCT country, " +
                "region1 AS region " +
                "FROM meme_dw.dim_country)AS c ON b.country = c.country) " +
                "group by 1,2) ");

        sqls.add("DELETE " +
                "FROM bi_dwd.dwd_wide_index_source_d " +
                "WHERE date = date(CURRENT_DATE - interval '1 day') " +
                "and index_desc = 'MonCashUser' ");

        sqls.add("INSERT INTO bi_dwd.dwd_wide_index_source_d " +
                "( " +
                "select " +
                "date(CURRENT_DATE - interval '1 day') as date, " +
                "region , " +
                "'MonCashUser' AS index_desc, " +
                "count(distinct uid) as RESULT " +
                "from " +
                "( " +
                "SELECT a.*, " +
                "c.region " +
                "FROM " +
                "(SELECT * " +
                "FROM work.funbank_meme_deposit " +
                "WHERE substring(date(ts),0,8) = substring(date(CURRENT_DATE - interval '1 day'),0,8)) a " +
                "JOIN fpsync.fp_user b ON a.uid = b.uid " +
                "LEFT JOIN " +
                "(SELECT DISTINCT country, " +
                "region1 AS region " +
                "FROM meme_dw.dim_country)AS c ON b.country = c.country) " +
                "group by 1,2)");

        sqls.add("DELETE " +
                "FROM bi_dwd.dwd_wide_index_source_d " +
                "WHERE date = date(CURRENT_DATE - interval '1 day') " +
                "and index_desc = 'MonCashArppu' ");

        sqls.add("INSERT INTO bi_dwd.dwd_wide_index_source_d " +
                "(select " +
                "date(CURRENT_DATE - interval '1 day') as date, " +
                "region , " +
                "'MonCashArppu' AS index_desc, " +
                "sum(amount)/count(distinct uid) as RESULT " +
                "from " +
                "( " +
                "SELECT a.*, " +
                "c.region " +
                "FROM " +
                "(SELECT * " +
                "FROM work.funbank_meme_deposit " +
                "WHERE substring(date(ts),0,8) = substring(date(CURRENT_DATE - interval '1 day'),0,8)) a " +
                "JOIN fpsync.fp_user b ON a.uid = b.uid " +
                "LEFT JOIN " +
                "(SELECT DISTINCT country, " +
                "region1 AS region " +
                "FROM meme_dw.dim_country)AS c ON b.country = c.country) " +
                "group by 1,2) ");

        sqls.add("DELETE " +
                "FROM bi_dwd.dwd_wide_index_source_d " +
                "WHERE date = date(CURRENT_DATE - interval '1 day') " +
                "and index_desc = 'MonCoinRevenuue' ");

        sqls.add("INSERT INTO bi_dwd.dwd_wide_index_source_d " +
                "( " +
                "select " +
                "date(CURRENT_DATE - interval '1 day') as date, " +
                "region, " +
                "'MonCoinRevenuue' AS index_desc, " +
                "sum(usd) as RESULT " +
                "from " +
                "( " +
                "SELECT a.*, " +
                "c.region " +
                "FROM " +
                "(SELECT * " +
                "FROM work.fifo_revenue " +
                "WHERE substring(day,0,8) = substring(date(CURRENT_DATE - interval '1 day'),0,8)) a " +
                "JOIN fpsync.fp_user b ON a.userid = b.uid " +
                "LEFT JOIN " +
                "(SELECT DISTINCT country, " +
                "region1 AS region " +
                "FROM meme_dw.dim_country)AS c ON b.country = c.country) " +
                "group by 1,2 " +
                ") ");

        sqls.add("DELETE " +
                "FROM bi_dwd.dwd_wide_index_source_d " +
                "WHERE date = date(CURRENT_DATE - interval '1 day') " +
                "and index_desc = 'MonCoinPoints' ");

        sqls.add("INSERT INTO bi_dwd.dwd_wide_index_source_d " +
                "( " +
                "select " +
                "date(CURRENT_DATE - interval '1 day') as date, " +
                "region, " +
                "'MonCoinPoints' AS index_desc, " +
                "sum(amount) as RESULT " +
                "from " +
                "( " +
                "SELECT a.*, " +
                "c.region " +
                "FROM " +
                "(SELECT * " +
                "FROM work.fifo_revenue " +
                "WHERE substring(day,0,8) = substring(date(CURRENT_DATE - interval '1 day'),0,8)) a " +
                "JOIN fpsync.fp_user b ON a.userid = b.uid " +
                "LEFT JOIN " +
                "(SELECT DISTINCT country, " +
                "region1 AS region " +
                "FROM meme_dw.dim_country)AS c ON b.country = c.country) " +
                "group by 1,2 " +
                ") ");

        sqls.add("DELETE " +
                "FROM bi_dwd.dwd_wide_index_source_d " +
                "WHERE date = date(CURRENT_DATE - interval '1 day') " +
                "and index_desc = 'MonCoinUser' ");

        sqls.add("INSERT INTO bi_dwd.dwd_wide_index_source_d " +
                "( " +
                "select " +
                "date(CURRENT_DATE - interval '1 day') as date, " +
                "region, " +
                "'MonCoinUser' AS index_desc, " +
                "count(distinct userid) as RESULT " +
                "from " +
                "( " +
                "SELECT a.*, " +
                "c.region " +
                "FROM " +
                "(SELECT * " +
                "FROM work.fifo_revenue " +
                "WHERE substring(day,0,8) = substring(date(CURRENT_DATE - interval '1 day'),0,8)) a " +
                "JOIN fpsync.fp_user b ON a.userid = b.uid " +
                "LEFT JOIN " +
                "(SELECT DISTINCT country, " +
                "region1 AS region " +
                "FROM meme_dw.dim_country)AS c ON b.country = c.country) " +
                "group by 1,2 " +
                ") ");

        sqls.add("DELETE " +
                "FROM bi_dwd.dwd_wide_index_source_d " +
                "WHERE date = date(CURRENT_DATE - interval '1 day') " +
                "and index_desc = 'MonCoinArppu' ");

        sqls.add("INSERT INTO bi_dwd.dwd_wide_index_source_d " +
                "( " +
                "select " +
                "date(CURRENT_DATE - interval '1 day') as date, " +
                "region, " +
                "'MonCoinArppu' AS index_desc, " +
                "sum(usd)/count(distinct userid) as RESULT " +
                "from " +
                "( " +
                "SELECT a.*, " +
                "c.region " +
                "FROM " +
                "(SELECT * " +
                "FROM work.fifo_revenue " +
                "WHERE substring(day,0,8) = substring(date(CURRENT_DATE - interval '1 day'),0,8)) a " +
                "JOIN fpsync.fp_user b ON a.userid = b.uid " +
                "LEFT JOIN " +
                "(SELECT DISTINCT country, " +
                "region1 AS region " +
                "FROM meme_dw.dim_country)AS c ON b.country = c.country) " +
                "group by 1,2 " +
                ") ");

        return sqls;
    }
}



