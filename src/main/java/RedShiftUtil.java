import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import util.RedShift_JDBCUtil;

import java.sql.Connection;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class RedShiftUtil {


    //static String url = "jdbc:redshift://meme-ap-bi.ccgysfvnoug7.us-west-2.redshift.amazonaws.com:5439/prod";
//    static String url = "jdbc:redshift://meme-ap-bi.ccgysfvnoug7.us-west-2.redshift.amazonaws.com:5439/prod";
//    static String user = "meme_pm_readonly";
//    static String pwd = "J2l7bt6lwNZgmAnq";
    static Logger runner_logger = LoggerFactory.getLogger("runner_logger");

    public static void main(String[] args) {
        String flag = args[0];
        if (args.length == 1) {
            System.out.println("start computer --> ");
            boolean data = runData(flag);
            System.out.println("task is successful");
            System.out.println("result----->" + data);
        } else {
            System.exit(0);
        }
    }


    /**
     * 埋点SQL
     *
     * @return
     */
    private static List<String> getRunSql_events() {
        List<String> sqls = new ArrayList<String>();
        sqls.add("delete from " +
                "    work.custom_events " +
                "where " +
                "    date(dt) >= date(current_date - interval '3 day')");
        sqls.add("insert into " +
                "    work.custom_events ( " +
                "        select " +
                "            * " +
                "        from " +
                "            raw_events.custom_events " +
                "        where " +
                "            date(dt) >= date(current_date - interval '3 day') " +
                "            and event_name in ( " +
                "                'translation_language', " +
                "                'live_room_game_teenpatti', " +
                "                'live_room_game_guess', " +
                "                'live_room_game_icefire', " +
                "                'live_room_game_redblue', " +
                "                'first_recharge_show', " +
                "                'first_recharge_confirm', " +
                "                'first_recharge_click', " +
                "                'first_recharge_confirm_success', " +
                "                'pet_magnifier_buy'," +
                "                'pet_room_click'," +
                "                'mora_click', " +
                "                'shaker_click', " +
                "                'new_ab_test', " +
                "                'message_send', " +
                "                'message_accpect', " +
                "                'newcomer_follow_cancel', " +
                "                'newcomer_follow_success', " +
                "                'newcomer_follow_end', " +
                "                'newcomer_guide_home', " +
                "                'newcomer_guide_live', " +
                "                'newcomer_gift_wish', " +
                "                'newcomer_guide_follow', " +
                "                'newcomer_guide_letter', " +
                "                'newcomer_sign_showroom', " +
                "                'pet_click', " +
                "                'pet_skin_click', " +
                "                'pet_skin_buy', " +
                "                'action_click', " +
                "                'action_buy', " +
                "                'pet_charge', " +
                "                'send_news_click', " +
                "                'active_game_click', " +
                "                'audience_list_click', " +
                "                'live_room_click', " +
                "                'anchor_card_click', " +
                "                'live_noble_click', " +
                "                'aristocrat_details_click', " +
                "                'hour_rank_click', " +
                "                'app_click', " +
                "                'newcomer_sign_getgift', " +
                "                'exposed_gifts_show', " +
                "                'exposed_gifts_click', " +
                "                'banner_gift', " +
                "                'me_click_1', " +
                "                'me_click_2', " +
                "                'me_click_3', " +
                "                'me_click_4', " +
                "                'me_click_5', " +
                "                'me_click_6', " +
                "                'me_click_7', " +
                "                'me_click_8', " +
                "                'me_click_9', " +
                "                'me_click_10', " +
                "                'me_click_11', " +
                "                'me_click_12', " +
                "                'me_click_13', " +
                "                'me_click_14', " +
                "                'me_click_15', " +
                "                'me_click_16', " +
                "                'me_click_17', " +
                "                'me_click_18', " +
                "                'me_click_19', " +
                "                'me_click_20', " +
                "                'me_click_21', " +
                "                'me_click_22', " +
                "                'other_click_1', " +
                "                'other_click_2', " +
                "                'other_click_3', " +
                "                'other_click_4', " +
                "                'other_click_5', " +
                "                'other_click_6', " +
                "                'other_click_7', " +
                "                'other_click_8', " +
                "                'session_start', " +
                "                'mall_start_button', " +
                "                'mall_click_button', " +
                "                'mall_confirm_button', " +
                "                'mall_success_button', " +
                "                'surprise_charge_show', " +
                "                'surprise_charge_click', " +
                "                'banner_click', " +
                "                'pk_kind', " +
                "                'push_click', " +
                "                'app_login_fail', " +
                "                'activity_click', " +
                "                'lottery_click', " +
                "                'app_enter_foreground', " +
                "                'homepage_show', " +
                "                'live_room_watching', " +
                "                'direct_message_sent', " +
                "                'invite_code_succ', " +
                "                'live_room_broadcasting', " +
                "                'app_login_button', " +
                "                'user_follow', " +
                "                'push_get', " +
                "                'app_login_window', " +
                "                'app_click', " +
                "                'live_room_start', " +
                "                'live_room_chat', " +
                "                'video_watch', " +
                "                'live_room_share_success', " +
                "                'video_button_click', " +
                "                'app_login_windows', " +
                "                'answer_click', " +
                "                'live_room_click', " +
                "                'live_room_share', " +
                "                'live_room_activity_simple_click', " +
                "                'live_room_like', " +
                "                'share_button_click', " +
                "                'app_login_success', " +
                "                'down_button_click', " +
                "                'showpage_button_click', " +
                "                'down_button_click', " +
                "                'right_status', " +
                "                'meme_login_button_click', " +
                "                'app_click', " +
                "                'invite_enter_click', " +
                "                'pet_magnifier_buy', " +
                "                'push_on' " +
                "            ) " +
                "    )");


        return sqls;
    }


    /**
     * 新版本elt——SQL
     *
     * @return
     */
    private static List<String> getRunSql_etl() {
        List<String> sqls = new ArrayList<String>();

        sqls.add("DROP TABLE IF EXISTS meme_dw.flow_source_adjust");
        sqls.add("create table meme_dw.flow_source_adjust as select " +
                "    *" +
                "from" +
                "    (" +
                "        select" +
                "            user_id," +
                "            tracker_name," +
                "            created_at," +
                "            country ad_country," +
                "            ROW_NUMBER() over(" +
                "                PARTITION BY user_id" +
                "                ORDER BY" +
                "                    created_at" +
                "            ) as nru" +
                "        from" +
                "            raw_events.adjust_events" +
                "        where" +
                "            (" +
                "                tracker_name <> ''" +
                "                and tracker_name is not null" +
                "            )" +
                "            and dt > '20200101'" +
                "    )" +
                "where" +
                "    nru = 1;");
        sqls.add("DROP TABLE IF EXISTS meme_dw.flow_source_event_tracking");
        sqls.add("create table meme_dw.flow_source_event_tracking as\n" +
                "SELECT " +
                "    * " +
                "FROM " +
                "    raw_events.custom_events " +
                "WHERE " +
                "    date(dt) >= date(CURRENT_DATE - interval '3 day') " +
                "    AND event_name IN ( " +
                "        'homepage_show', " +
                "        'app_enter_foreground', " +
                "        'app_click' " +
                "    )");
        sqls.add("delete from " +
                "    meme_dw.flow_source_tmp_dau_pre " +
                "where " +
                "    date(dt) >= date(current_date - interval '3 day')");
        sqls.add("insert into " +
                "    meme_dw.flow_source_tmp_dau_pre ( " +
                "        select " +
                "            device_id, " +
                "            uid, " +
                "            ts, " +
                "            dt, " +
                "            install_ts, " +
                "            app_version, " +
                "            os, " +
                "            os_version, " +
                "            country, " +
                "            city, " +
                "            str_item_3 AS type " +
                "        from " +
                "            meme_dw.flow_source_event_tracking " +
                "    )");
        sqls.add("DROP TABLE IF EXISTS meme_dw.flow_source_dau");
        sqls.add("CREATE TABLE meme_dw.flow_source_dau AS " +
                "SELECT " +
                "    * " +
                "FROM " +
                "    ( " +
                "        SELECT " +
                "            *, " +
                "            ROW_NUMBER() over( " +
                "                PARTITION BY device_id, " +
                "                date(ts) " +
                "                ORDER BY " +
                "                    ts " +
                "            ) as dau, " +
                "            ROW_NUMBER() over( " +
                "                PARTITION BY device_id " +
                "                ORDER BY " +
                "                    ts " +
                "            ) as nru " +
                "        FROM " +
                "            meme_dw.flow_source_tmp_dau_pre " +
                "    ) " +
                "WHERE " +
                "    dau = 1");
        sqls.add("DROP TABLE IF EXISTS meme_dw.flow_source_dau_fpid");
        sqls.add("CREATE TABLE meme_dw.flow_source_dau_fpid AS " +
                "SELECT " +
                "    *, " +
                "case " +
                "        when os = 'android' then type " +
                "        else device_id " +
                "    end fpid " +
                "from " +
                "    meme_dw.flow_source_dau");

        sqls.add("DROP TABLE IF EXISTS meme_dw.flow_source_dau_result");

        sqls.add("CREATE TABLE meme_dw.flow_source_dau_result AS select  " +
                "    a.*,  " +
                "    split_part(b.tracker_name, '::', 1) as install_source,  " +
                "    nullif(split_part(b.tracker_name, '::', 2), '') as install_campaign,  " +
                "    nullif(split_part(b.tracker_name, '::', 3), '') as install_subpublisher,  " +
                "    nullif(split_part(b.tracker_name, '::', 4), '') as install_creative_id,  " +
                "    case  " +
                "        when country in('PERKY', 'JP') then '5—JP'  " +
                "        when country in('CN', ' CN') then '2—CN'  " +
                "        when country in (  " +
                "            ' TW',  " +
                "            'TW',  " +
                "            'SG',  " +
                "            'CA',  " +
                "            'AU',  " +
                "            'US',  " +
                "            'MY',  " +
                "            'HK',  " +
                "            'MO',  " +
                "            'KR'  " +
                "        ) then '1—TW'  " +
                "        when country = 'ABCDEFGHIJKLMN' then '6-others'  " +
                "        when country in('IN', ' PK') then '3—IN'  " +
                "        else '4—mean'  " +
                "    end region,  " +
                "    case  " +
                "        when country in('PERKY', 'JP') then '4_JP'  " +
                "        when country in('CN', ' CN') then '2_CN'  " +
                "        when country in (  " +
                "            ' TW',  " +
                "            'TW',  " +
                "            'SG',  " +
                "            'CA',  " +
                "            'AU',  " +
                "            'US',  " +
                "            'MY',  " +
                "            'HK',  " +
                "            'MO',  " +
                "            'KR'  " +
                "        ) then '1_TW'  " +
                "        when country = 'ABCDEFGHIJKLMN' then '5-others'  " +
                "        else '3_IN'  " +
                "    end region2,  " +
                "    case  " +
                "        when country in('PERKY', 'JP') then 'JP'  " +
                "        when country in('CN', ' CN') then 'CN'  " +
                "        when country in (  " +
                "            ' TW',  " +
                "            'TW',  " +
                "            'SG',  " +
                "            'CA',  " +
                "            'AU',  " +
                "            'US',  " +
                "            'MY',  " +
                "            'HK',  " +
                "            'MO',  " +
                "            'KR'  " +
                "        ) then 'TW'  " +
                "        when country = 'ABCDEFGHIJKLMN' then 'OTHERS'  " +
                "        when country in('IN', ' PK') then 'IN'  " +
                "        else 'MEAN'  " +
                "    end region6,  " +
                "    case  " +
                "        when country in('PERKY', 'JP') then 'JP'  " +
                "        when country in('CN', ' CN') then 'CN'  " +
                "        when country in (  " +
                "            ' TW',  " +
                "            'TW',  " +
                "            'SG',  " +
                "            'CA',  " +
                "            'AU',  " +
                "            'US',  " +
                "            'MY',  " +
                "            'HK',  " +
                "            'MO',  " +
                "            'KR'  " +
                "        ) then 'TW'  " +
                "        when country = 'ABCDEFGHIJKLMN' then 'OTHERS'  " +
                "        else 'IN'  " +
                "    end region5  " +
                "from  " +
                "    meme_dw.flow_source_dau_fpid a  " +
                "    left join meme_dw.flow_source_adjust b on a.fpid = b.user_id");
        sqls.add("update  " +
                "    meme_dw.flow_source_dau_result  " +
                "SET  " +
                "    region = '6-others',  " +
                "    region2 = '5-others',  " +
                "    region6 = 'others',  " +
                "    region5 = 'others'  " +
                "where  " +
                "    date(ts) >= '2021-01-01'  " +
                "    and install_source = 'AND-IND-Batmobi-0326'  " +
                "    and (  " +
                "        region = '2—CN'  " +
                "        or region2 = '2_CN'  " +
                "        or region5 = 'CN'  " +
                "        or region6 = 'CN'  " +
                "    )");



        return sqls;
    }


    /**
     * 充值表SQL
     */
    private static List<String> getRunSql_deposit() {
        List<String> sqls = new ArrayList<String>();

        sqls.add("delete from " +
                "    work.funbank_meme_deposit " +
                "where " +
                "    date(ts) >= date(current_date - interval '3 day')");

        sqls.add("insert into " +
                "    work.funbank_meme_deposit ( " +
                "        select " +
                "            transaction_id, " +
                "            external_id, " +
                "            gateway, " +
                "            a.uid uid, " +
                "            account_name, " +
                "            package_id, " +
                "            amount, " +
                "            currency_id, " +
                "            ts, " +
                "            mcoin, " +
                "            user_id, " +
                "            app_id, " +
                "            os, " +
                "            nick, " +
                "            country " +
                "        from " +
                "            ( " +
                "                select " +
                "                    transaction_id, " +
                "                    external_id, " +
                "                    gateway, " +
                "                    uid, " +
                "                    account_name, " +
                "                    package_id, " +
                "                    amount * factor amount, " +
                "                    'USD' currency_id, " +
                "                    ts, " +
                "                    mcoin " +
                "                from " +
                "                    ( " +
                "                        select " +
                "                            transaction_id, " +
                "                            external_id, " +
                "                            gateway, " +
                "                            uid, " +
                "                            account_name, " +
                "                            package_id, " +
                "                            amount, " +
                "                            currency_id, " +
                "                            ts, " +
                "                            mcoin, " +
                "                            date(ts) dt " +
                "                        from " +
                "                            meme.funbank_meme_deposit " +
                "                        where " +
                "                            gateway not in ('system', 'appleiap') " +
                "                            and date(ts) >= date(current_date - interval '3 day') " +
                "                            and package_id not like 'manualpay-prestored-%' " +
                "                        union " +
                "                        select " +
                "                            transaction_id, " +
                "                            external_id, " +
                "                            gateway, " +
                "                            uid, " +
                "                            account_name, " +
                "                            package_id, " +
                "                            amount, " +
                "                            currency_id, " +
                "                            ts, " +
                "                            mcoin, " +
                "                            date(ts) dt " +
                "                        from " +
                "                            meme.funbank_meme_deposit " +
                "                        where " +
                "                            gateway in ('appleiap') " +
                "                            and date(ts) >= date(current_date - interval '3 day') " +
                "                            and date(ts) >= '2020-01-01' " +
                "                            and package_id not like 'manualpay-prestored-%' " +
                "                    ) a " +
                "                    join ( " +
                "                        select " +
                "                            distinct dt, " +
                "                            currency currency_id, " +
                "                            factor " +
                "                        from " +
                "                            kpi.currency " +
                "                    ) b on a.currency_id = b.currency_id " +
                "                    and a.dt = b.dt " +
                "                union " +
                "                select " +
                "                    transaction_id, " +
                "                    external_id, " +
                "                    gateway, " +
                "                    uid, " +
                "                    account_name, " +
                "                    a.package_id package_id, " +
                "                    usd amount, " +
                "                    'USD' currency_id, " +
                "                    ts, " +
                "                    mcoin " +
                "                from " +
                "                    ( " +
                "                        select " +
                "                            transaction_id, " +
                "                            external_id, " +
                "                            gateway, " +
                "                            uid, " +
                "                            account_name, " +
                "                            package_id, " +
                "                            amount, " +
                "                            currency_id, " +
                "                            ts, " +
                "                            mcoin " +
                "                        from " +
                "                            meme.funbank_meme_deposit " +
                "                        where " +
                "                            gateway = 'appleiap' " +
                "                            and date(ts) >= date(current_date - interval '3 day') " +
                "                            and package_id not like 'manualpay-prestored-%' " +
                "                            and date(ts) <= '2020-01-01' " +
                "                    ) a " +
                "                    join ( " +
                "                        select " +
                "                            package_id, " +
                "                            avg(money * factor) usd " +
                "                        from " +
                "                            ( " +
                "                                select " +
                "                                    distinct date(ts) dt, " +
                "                                    currency_id, " +
                "                                    amount money, " +
                "                                    transaction_id, " +
                "                                    uid, " +
                "                                    gateway, " +
                "                                    package_id, " +
                "                                    mcoin, " +
                "                                    external_id, " +
                "                                    amount, " +
                "                                    ts " +
                "                                from " +
                "                                    meme.funbank_meme_deposit " +
                "                                where " +
                "                                    gateway = 'appleiap' " +
                "                                    and date(ts) <= '2018-10-01' " +
                "                                    and date(ts) >= '2018-01-01' " +
                "                            ) a " +
                "                            join ( " +
                "                                select " +
                "                                    distinct dt, " +
                "                                    currency currency_id, " +
                "                                    factor " +
                "                                from " +
                "                                    kpi.currency " +
                "                            ) b on a.currency_id = b.currency_id " +
                "                            and a.dt = b.dt " +
                "                        group by " +
                "                            package_id " +
                "                    ) b on a.package_id = b.package_id " +
                "            ) a " +
                "            left join ( " +
                "                select " +
                "                    uid, " +
                "                    user_id, " +
                "                    app_id, " +
                "                    os " +
                "                from " +
                "                    work.uid_device_app_new " +
                "            ) b on a.uid = b.uid " +
                "            left join ( " +
                "                select " +
                "                    distinct uid, " +
                "                    nick, " +
                "                    country " +
                "                from " +
                "                    fpsync.fp_user " +
                "            ) c on a.uid = c.uid " +
                "    )");
        return sqls;
    }


    /**
     * 确认收入数据SQL
     *
     * @return
     */
    private static List<String> getRunSql_revenue() {
        List<String> sqls = new ArrayList<String>();

        sqls.add("drop table work.fifo_revenue");
        sqls.add("CREATE TABLE work.fifo_revenue as " +
                "select " +
                "  cast(consume_type as varchar) as type, " +
                "  date(trans_time) as day, " +
                "  amount, " +
                "  value as usd, " +
                "  0 as id, " +
                "  trans_id as transactionid, " +
                "  external_id as externalid, " +
                "  debit_user as userid, " +
                "  value as value, " +
                "  '' as memo, " +
                "  0 as status, " +
                "  0 as createdat, " +
                "  '' as details, " +
                "  result_user, " +
                "  create_user, " +
                "  debit_user, " +
                "  credit_user, " +
                "  trans_time as ts " +
                "from " +
                "  ( " +
                "    select " +
                "      *, " +
                "case " +
                "        when create_user = '' then debit_user " +
                "        else create_user " +
                "      end result_user " +
                "    from " +
                "      newbank.income_confirm " +
                "  ) " +
                "union " +
                "all " +
                "select " +
                "  *, " +
                "  userid result_user, " +
                "  userid create_user, " +
                "  '' debit_user, " +
                "  '' credit_user, " +
                "  '1999-01-01 00:00:00' as ts " +
                "from " +
                "  work.fifo_revenue_bak_0731");
        sqls.add("grant select on table work.fifo_revenue to meme_bi_dev");


        return sqls;
    }


    private static boolean runData(String flag) {
        List<String> runSqls = new ArrayList<>();

        if (flag.equals("etl")) {
            runSqls = getRunSql_etl();
            System.out.println("=============etl is successful=============");
        } else if (flag.equals("fifo_revenue")) {
            runSqls = getRunSql_revenue();
            System.out.println("=============fifo_revenue is successful=============");
        } else if (flag.equals("funbank_meme_deposit")) {
            runSqls = getRunSql_deposit();
            System.out.println("=============funbank_meme_deposit is successful=============");
        } else if (flag.equals("event")) {
            runSqls = getRunSql_events();
            System.out.println("=============event is successful=============");
        } else {
            runner_logger.error("args error!!!");
        }


        Connection connection = null;
        try {
            connection = RedShift_JDBCUtil.getConnection();

            for (String sql : runSqls) {
                Statement statement = connection.createStatement();
                statement.execute(sql);
                statement.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        } finally {
            try {
                if (connection == null) {
                    connection.close();
                }
            } catch (Exception e) {
            }
        }
        return true;
    }


    /**
     * 获取链接
     *
     * @return
     * @throws Exception
     */
//    public static Connection getConnection() throws Exception {
//        //jdbc.properties一般是放在src目录下的配置文件具体内容下面给出
//        DataSource dataSource = new com.amazon.redshift.jdbc42.DataSource();
//        dataSource.setURL(url);
//        return dataSource.getConnection(user, pwd);
//    }

}
